#!/bin/bash

# start db
#From http://stackoverflow.com/questions/29145370/docker-initialize-mysql-database-with-schema

set -e

# first, if the /var/lib/mysql directory is empty, unpack it from our predefined db
[ "$(ls -A /var/lib/mysql)" ] && echo "Running with existing database in /var/lib/mysql" || ( echo 'Populate initial db'; tar xpzvf default_mysql.tar.gz )

/usr/sbin/mysqld
