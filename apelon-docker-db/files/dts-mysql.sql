-- MySQL dump 10.13  Distrib 5.6.27, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: dts
-- ------------------------------------------------------
-- Server version	5.6.27-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE `dts`;
USE `dts`;

--
-- Table structure for table `APELON_VERSION`
--

DROP TABLE IF EXISTS `APELON_VERSION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `APELON_VERSION` (
  `app` varchar(32) NOT NULL,
  `version` varchar(32) NOT NULL,
  PRIMARY KEY (`app`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CONTENT_LICENSE`
--

DROP TABLE IF EXISTS `CONTENT_LICENSE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CONTENT_LICENSE` (
  `namespace_id` int(11) DEFAULT NULL,
  `license_text` tinyblob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DIRECT_SUPS_PLUS`
--

DROP TABLE IF EXISTS `DIRECT_SUPS_PLUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DIRECT_SUPS_PLUS` (
  `con` bigint(20) NOT NULL,
  `sup` bigint(20) NOT NULL,
  PRIMARY KEY (`con`,`sup`),
  KEY `direct_sups_plus_con_idx` (`con`),
  KEY `direct_sups_plus_sup_idx` (`sup`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_ASSOCIATION_TYPE`
--

DROP TABLE IF EXISTS `DTS_ASSOCIATION_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_ASSOCIATION_TYPE` (
  `type_gid` bigint(20) NOT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `connects` char(1) NOT NULL,
  `purpose` char(1) NOT NULL,
  `inverse_name` varchar(256) DEFAULT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`type_gid`),
  UNIQUE KEY `unique_association_code` (`namespace_id`,`code`),
  UNIQUE KEY `unique_association_id` (`namespace_id`,`id`),
  UNIQUE KEY `unique_association_name` (`namespace_id`,`name`(255)),
  CONSTRAINT `association_namespace_fk` FOREIGN KEY (`namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_ASSOCIATION_TYPE_ARCHIVE`
--

DROP TABLE IF EXISTS `DTS_ASSOCIATION_TYPE_ARCHIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_ASSOCIATION_TYPE_ARCHIVE` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `type_gid` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `connects` char(1) NOT NULL,
  `purpose` char(1) NOT NULL,
  `inverse_name` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `arch_assoc_type_vin_fk` (`version_in`),
  KEY `arch_assoc_type_vout_fk` (`version_out`),
  CONSTRAINT `arch_assoc_type_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `arch_assoc_type_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Authority`
--

DROP TABLE IF EXISTS `DTS_Authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Authority` (
  `id` int(11) NOT NULL,
  `authority_desc` varchar(4000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_BIG_CONCEPT_PROPERTY`
--

DROP TABLE IF EXISTS `DTS_BIG_CONCEPT_PROPERTY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_BIG_CONCEPT_PROPERTY` (
  `concept_property_iid` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `property_gid` bigint(20) NOT NULL,
  `value` tinyblob NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`concept_property_iid`),
  KEY `dts_big_prop_cgid_idx` (`concept_gid`),
  KEY `big_con_prop_pfk` (`property_gid`),
  CONSTRAINT `big_con_prop_cfk` FOREIGN KEY (`concept_gid`) REFERENCES `DTS_Concept` (`concept_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `big_con_prop_pfk` FOREIGN KEY (`property_gid`) REFERENCES `DTS_PROPERTY_TYPE` (`type_gid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_BIG_CON_PROP_ARCH`
--

DROP TABLE IF EXISTS `DTS_BIG_CON_PROP_ARCH`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_BIG_CON_PROP_ARCH` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `concept_property_iid` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `property_gid` bigint(20) NOT NULL,
  `value` tinyblob NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `dts_big_prop_arch_cgid_idx` (`concept_gid`),
  KEY `big_con_prop_vin_fk` (`version_in`),
  KEY `big_con_prop_vout_fk` (`version_out`),
  CONSTRAINT `big_con_prop_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `big_con_prop_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_CLASSIFIER_GRAPH`
--

DROP TABLE IF EXISTS `DTS_CLASSIFIER_GRAPH`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_CLASSIFIER_GRAPH` (
  `namespace_id` int(11) NOT NULL,
  `serialized_classifier` tinyblob NOT NULL,
  `version` varchar(256) NOT NULL,
  PRIMARY KEY (`namespace_id`),
  CONSTRAINT `graph_namespace_fk` FOREIGN KEY (`namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_CLASSIFY_CYCLE_ERROR`
--

DROP TABLE IF EXISTS `DTS_CLASSIFY_CYCLE_ERROR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_CLASSIFY_CYCLE_ERROR` (
  `id` int(11) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `role_gid` bigint(20) DEFAULT NULL,
  `classified_namespace_id` int(11) NOT NULL,
  KEY `dts_classify_cycle_err_ns_idx` (`classified_namespace_id`),
  CONSTRAINT `cycle_error_namespace_fk` FOREIGN KEY (`classified_namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_CLASSIFY_EQ_ERROR`
--

DROP TABLE IF EXISTS `DTS_CLASSIFY_EQ_ERROR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_CLASSIFY_EQ_ERROR` (
  `concept1_gid` bigint(20) NOT NULL,
  `concept2_gid` bigint(20) NOT NULL,
  `classified_namespace_id` int(11) NOT NULL,
  KEY `dts_classify_eq_err_ns_idx` (`classified_namespace_id`),
  CONSTRAINT `eq_error_namespace_fk` FOREIGN KEY (`classified_namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_CLASSIFY_MONITOR`
--

DROP TABLE IF EXISTS `DTS_CLASSIFY_MONITOR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_CLASSIFY_MONITOR` (
  `namespace_id` int(11) NOT NULL,
  `classify_required` char(1) NOT NULL,
  `last_classified` datetime(6) NOT NULL,
  PRIMARY KEY (`namespace_id`),
  CONSTRAINT `monitor_namespace_fk` FOREIGN KEY (`namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_COMPLETE_ROLE_CON`
--

DROP TABLE IF EXISTS `DTS_COMPLETE_ROLE_CON`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_COMPLETE_ROLE_CON` (
  `role_iid` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `role_gid` bigint(20) NOT NULL,
  `value_concept_gid` bigint(20) NOT NULL,
  `modifier_id` int(11) NOT NULL,
  `rolegroup` int(11) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`role_iid`),
  KEY `dts_compl_role_con_cid_idx` (`concept_gid`),
  KEY `dts_compl_role_con_role_idx` (`role_gid`),
  KEY `dts_compl_role_con_vcid_idx` (`value_concept_gid`),
  KEY `dts_compl_role_cvr_idx` (`concept_gid`,`value_concept_gid`,`role_gid`),
  KEY `dts_compl_role_vr_idx` (`value_concept_gid`,`role_gid`),
  CONSTRAINT `complete_role_con_cfk` FOREIGN KEY (`concept_gid`) REFERENCES `DTS_Concept` (`concept_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `complete_role_con_rfk` FOREIGN KEY (`role_gid`) REFERENCES `DTS_ROLE_TYPE` (`type_gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `complete_role_con_vfk` FOREIGN KEY (`value_concept_gid`) REFERENCES `DTS_Concept` (`concept_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_COMPLETE_ROLE_CON_ARCHIVE`
--

DROP TABLE IF EXISTS `DTS_COMPLETE_ROLE_CON_ARCHIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_COMPLETE_ROLE_CON_ARCHIVE` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `role_iid` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `role_gid` bigint(20) NOT NULL,
  `value_concept_gid` bigint(20) NOT NULL,
  `modifier_id` int(11) NOT NULL,
  `rolegroup` int(11) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `dts_cp_role_con_arch_cid_idx` (`concept_gid`),
  KEY `dts_cp_role_con_arch_role_idx` (`role_gid`),
  KEY `dts_cp_role_con_arch_vcid_idx` (`value_concept_gid`),
  KEY `arch_complete_role_vin_fk` (`version_in`),
  KEY `arch_complete_role_vout_fk` (`version_out`),
  CONSTRAINT `arch_complete_role_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `arch_complete_role_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_CONCEPT_ARCHIVE`
--

DROP TABLE IF EXISTS `DTS_CONCEPT_ARCHIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_CONCEPT_ARCHIVE` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(749) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `kind_gid` bigint(20) DEFAULT NULL,
  `primitive` char(1) DEFAULT NULL,
  `value_u` varchar(749) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `dts_con_arch_id_idx` (`id`),
  KEY `dts_con_arch_code_idx` (`code`),
  KEY `dts_con_arch_name_idx` (`name`(255)),
  KEY `dts_con_arch_nmsp_idx` (`namespace_id`),
  KEY `dts_con_arch_kind_idx` (`kind_gid`),
  KEY `dts_con_arch_value_u_idx` (`value_u`(255)),
  KEY `concept_vin_fk` (`version_in`),
  KEY `concept_vout_fk` (`version_out`),
  CONSTRAINT `concept_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `concept_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_CONCEPT_ASSN_ARCHIVE`
--

DROP TABLE IF EXISTS `DTS_CONCEPT_ASSN_ARCHIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_CONCEPT_ASSN_ARCHIVE` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `association_iid` bigint(20) NOT NULL,
  `from_concept_gid` bigint(20) NOT NULL,
  `association_gid` bigint(20) NOT NULL,
  `to_concept_gid` bigint(20) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `CON_ASSN_ARCH_ASSO_GID_IDX` (`association_gid`),
  KEY `CON_ASSN_ARCH_FROM_CON_IDX` (`from_concept_gid`),
  KEY `CON_ASSN_ARCH_TO_CON_IDX` (`to_concept_gid`),
  KEY `arch_concept_assn_vin_fk` (`version_in`),
  KEY `arch_concept_assn_vout_fk` (`version_out`),
  CONSTRAINT `arch_concept_assn_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `arch_concept_assn_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_CONCEPT_ASSOCIATION`
--

DROP TABLE IF EXISTS `DTS_CONCEPT_ASSOCIATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_CONCEPT_ASSOCIATION` (
  `association_iid` bigint(20) NOT NULL,
  `from_concept_gid` bigint(20) NOT NULL,
  `association_gid` bigint(20) NOT NULL,
  `to_concept_gid` bigint(20) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`association_iid`),
  UNIQUE KEY `unique_con_assoc` (`from_concept_gid`,`association_gid`,`to_concept_gid`),
  KEY `con_assn_asso_gid_idx` (`association_gid`),
  KEY `con_assn_from_con_idx` (`from_concept_gid`),
  KEY `con_assn_to_con_idx` (`to_concept_gid`),
  CONSTRAINT `concept_assn_afk` FOREIGN KEY (`association_gid`) REFERENCES `DTS_ASSOCIATION_TYPE` (`type_gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `concept_assn_from_cfk` FOREIGN KEY (`from_concept_gid`) REFERENCES `DTS_Concept` (`concept_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `concept_assn_to_cfk` FOREIGN KEY (`to_concept_gid`) REFERENCES `DTS_Concept` (`concept_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Concept`
--

DROP TABLE IF EXISTS `DTS_Concept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Concept` (
  `concept_gid` bigint(20) NOT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(749) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `kind_gid` bigint(20) DEFAULT NULL,
  `primitive` char(1) DEFAULT NULL,
  `value_u` varchar(749) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`concept_gid`),
  UNIQUE KEY `unique_concept_code` (`namespace_id`,`code`),
  UNIQUE KEY `unique_concept_id` (`namespace_id`,`id`),
  UNIQUE KEY `unique_concept_name` (`namespace_id`,`name`(255)),
  KEY `dts_con_id_idx` (`id`),
  KEY `dts_con_code_idx` (`code`),
  KEY `dts_con_name_idx` (`name`(255)),
  KEY `dts_con_kind_idx` (`kind_gid`),
  KEY `dts_con_value_u_idx` (`value_u`(255)),
  KEY `dts_concept_gv_idx` (`concept_gid`,`value_u`(255)),
  KEY `dts_con_nmsp_idx` (`namespace_id`),
  CONSTRAINT `concept_kind_fk` FOREIGN KEY (`kind_gid`) REFERENCES `DTS_Kind` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `concept_namespace_fk` FOREIGN KEY (`namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_DEFINING_CONCEPT_ARCHIVE`
--

DROP TABLE IF EXISTS `DTS_DEFINING_CONCEPT_ARCHIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_DEFINING_CONCEPT_ARCHIVE` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `superconcept_gid` bigint(20) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `DEF_CON_ARCH_CON_GID_IDX` (`concept_gid`),
  KEY `DEF_CON_ARCH_SUP_GID_IDX` (`superconcept_gid`),
  KEY `arch_type_concept_vin_fk` (`version_in`),
  KEY `arch_type_concept_vout_fk` (`version_out`),
  CONSTRAINT `arch_type_concept_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `arch_type_concept_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_DEFINING_ROLE`
--

DROP TABLE IF EXISTS `DTS_DEFINING_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_DEFINING_ROLE` (
  `role_iid` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `role_gid` bigint(20) NOT NULL,
  `value_concept_gid` bigint(20) NOT NULL,
  `modifier_id` int(11) NOT NULL,
  `rolegroup` int(11) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`role_iid`),
  KEY `dts_defining_role_con_idx` (`concept_gid`),
  KEY `dts_defining_role_role_idx` (`role_gid`),
  KEY `dts_defining_role_vcid_idx` (`value_concept_gid`),
  CONSTRAINT `defining_role_cfk` FOREIGN KEY (`concept_gid`) REFERENCES `DTS_Concept` (`concept_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `defining_role_rfk` FOREIGN KEY (`role_gid`) REFERENCES `DTS_ROLE_TYPE` (`type_gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `defining_role_vfk` FOREIGN KEY (`value_concept_gid`) REFERENCES `DTS_Concept` (`concept_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_DIRECT_SUPS_ARCHIVE`
--

DROP TABLE IF EXISTS `DTS_DIRECT_SUPS_ARCHIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_DIRECT_SUPS_ARCHIVE` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `superconcept_gid` bigint(20) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `dts_direct_sups_arch_cid_idx` (`concept_gid`),
  KEY `dts_direct_sups_arch_sid_idx` (`superconcept_gid`),
  KEY `arch_direct_sups_vin_fk` (`version_in`),
  KEY `arch_direct_sups_vout_fk` (`version_out`),
  CONSTRAINT `arch_direct_sups_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `arch_direct_sups_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Defining_Concept`
--

DROP TABLE IF EXISTS `DTS_Defining_Concept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Defining_Concept` (
  `concept_gid` bigint(20) NOT NULL,
  `superconcept_gid` bigint(20) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`concept_gid`,`superconcept_gid`),
  KEY `dts_defining_concept_con_idx` (`concept_gid`),
  KEY `dts_defining_concept_sup_idx` (`superconcept_gid`),
  CONSTRAINT `defining_concept_cfk` FOREIGN KEY (`concept_gid`) REFERENCES `DTS_Concept` (`concept_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Defining_Role_Archive`
--

DROP TABLE IF EXISTS `DTS_Defining_Role_Archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Defining_Role_Archive` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `role_iid` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `role_gid` bigint(20) NOT NULL,
  `value_concept_gid` bigint(20) NOT NULL,
  `modifier_id` int(11) NOT NULL,
  `rolegroup` int(11) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `dts_def_role_archive_con_idx` (`concept_gid`),
  KEY `dts_def_role_archive_role_idx` (`role_gid`),
  KEY `dts_def_role_archive_vcid_idx` (`value_concept_gid`),
  KEY `arch_type_role_vin_fk` (`version_in`),
  KEY `arch_type_role_vout_fk` (`version_out`),
  CONSTRAINT `arch_type_role_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `arch_type_role_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Direct_Sups`
--

DROP TABLE IF EXISTS `DTS_Direct_Sups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Direct_Sups` (
  `concept_gid` bigint(20) NOT NULL,
  `superconcept_gid` bigint(20) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  `namespace_id` int(11) NOT NULL,
  PRIMARY KEY (`concept_gid`,`superconcept_gid`),
  KEY `dts_direct_sups_cid_idx` (`concept_gid`),
  KEY `dts_direct_sups_sid_idx` (`superconcept_gid`),
  CONSTRAINT `direct_sups_cfk` FOREIGN KEY (`concept_gid`) REFERENCES `DTS_Concept` (`concept_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_IMPORT_STATUS`
--

DROP TABLE IF EXISTS `DTS_IMPORT_STATUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_IMPORT_STATUS` (
  `NAMESPACE_ID` int(11) NOT NULL,
  `VERSION_ID` int(11) NOT NULL,
  `TABLE_NAME` varchar(32) NOT NULL,
  `LOAD_STATUS` varchar(50) NOT NULL,
  `IMPORT_TYPE` varchar(10) NOT NULL,
  `FILE_LOCATION` varchar(250) NOT NULL,
  `TOTAL_FILE_LINES` int(11) NOT NULL,
  `LINES_IMPORTED` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_INDEXABLE_CONCEPT_PROPERTY`
--

DROP TABLE IF EXISTS `DTS_INDEXABLE_CONCEPT_PROPERTY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_INDEXABLE_CONCEPT_PROPERTY` (
  `concept_property_iid` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `property_gid` bigint(20) NOT NULL,
  `value` varchar(749) NOT NULL,
  `value_u` varchar(749) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`concept_property_iid`),
  KEY `dts_index_prop_cgid_idx` (`concept_gid`),
  KEY `dts_index_prop_pgid_idx` (`property_gid`),
  KEY `dts_index_prop_val_idx` (`value`(255)),
  KEY `dts_index_prop_val_u_idx` (`value_u`(255)),
  CONSTRAINT `index_con_prop_cfk` FOREIGN KEY (`concept_gid`) REFERENCES `DTS_Concept` (`concept_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `index_con_prop_pfk` FOREIGN KEY (`property_gid`) REFERENCES `DTS_PROPERTY_TYPE` (`type_gid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_INDEXABLE_CON_PROP_ARCHIVE`
--

DROP TABLE IF EXISTS `DTS_INDEXABLE_CON_PROP_ARCHIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_INDEXABLE_CON_PROP_ARCHIVE` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `concept_property_iid` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `property_gid` bigint(20) NOT NULL,
  `value` varchar(749) NOT NULL,
  `value_u` varchar(749) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `dts_index_prop_arch_cgid_idx` (`concept_gid`),
  KEY `dts_index_prop_arch_pgid_idx` (`property_gid`),
  KEY `dts_index_prop_arch_val_idx` (`value`(255)),
  KEY `dts_index_prop_arch_val_u_idx` (`value_u`(255)),
  KEY `indexable_con_prop_vin_fk` (`version_in`),
  KEY `indexable_con_prop_vout_fk` (`version_out`),
  CONSTRAINT `indexable_con_prop_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `indexable_con_prop_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Kind`
--

DROP TABLE IF EXISTS `DTS_Kind`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Kind` (
  `gid` bigint(20) NOT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `reference` char(1) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`gid`),
  UNIQUE KEY `unique_kind_code` (`namespace_id`,`code`),
  UNIQUE KEY `unique_kind_id` (`namespace_id`,`id`),
  UNIQUE KEY `unique_kind_name` (`namespace_id`,`name`(255)),
  CONSTRAINT `kind_namespace_fk` FOREIGN KEY (`namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Kind_Archive`
--

DROP TABLE IF EXISTS `DTS_Kind_Archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Kind_Archive` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `gid` bigint(20) NOT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `reference` char(1) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `kind_type_vin_fk` (`version_in`),
  KEY `kind_type_vout_fk` (`version_out`),
  CONSTRAINT `kind_type_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `kind_type_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_NAMESPACE_LINKAGE`
--

DROP TABLE IF EXISTS `DTS_NAMESPACE_LINKAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_NAMESPACE_LINKAGE` (
  `namespace_id` int(11) NOT NULL,
  `subs_namespace_id` int(11) NOT NULL,
  `subs_version` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`namespace_id`),
  KEY `subsnamespace_fk` (`subs_namespace_id`),
  CONSTRAINT `linkage_namespace_fk` FOREIGN KEY (`namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `subsnamespace_fk` FOREIGN KEY (`subs_namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Namespace`
--

DROP TABLE IF EXISTS `DTS_Namespace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Namespace` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `code` varchar(32) NOT NULL,
  `referenced_by` char(1) NOT NULL,
  `authority_id` int(11) NOT NULL,
  `is_local` char(1) NOT NULL,
  `is_writeable` char(1) NOT NULL,
  `has_semantic_types` char(1) NOT NULL,
  `namespace_type` char(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_namespace_code` (`code`),
  UNIQUE KEY `unique_namespace_name` (`name`(255)),
  KEY `authority_fk` (`authority_id`),
  CONSTRAINT `authority_fk` FOREIGN KEY (`authority_id`) REFERENCES `DTS_Authority` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_PROPERTY_TYPE`
--

DROP TABLE IF EXISTS `DTS_PROPERTY_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_PROPERTY_TYPE` (
  `type_gid` bigint(20) NOT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `value_size` char(1) NOT NULL,
  `attaches_to` char(1) NOT NULL,
  `contains_index` char(1) DEFAULT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`type_gid`),
  UNIQUE KEY `unique_property_code` (`namespace_id`,`code`),
  UNIQUE KEY `unique_property_id` (`namespace_id`,`id`),
  UNIQUE KEY `unique_property_name` (`namespace_id`,`name`(255)),
  CONSTRAINT `property_namespace_fk` FOREIGN KEY (`namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_PROPERTY_TYPE_ARCHIVE`
--

DROP TABLE IF EXISTS `DTS_PROPERTY_TYPE_ARCHIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_PROPERTY_TYPE_ARCHIVE` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `type_gid` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `value_size` char(1) NOT NULL,
  `attaches_to` char(1) NOT NULL,
  `contains_index` char(1) DEFAULT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `arch_property_type_vin_fk` (`version_in`),
  KEY `arch_property_type_vout_fk` (`version_out`),
  CONSTRAINT `arch_property_type_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `arch_property_type_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_QUALIFIED_INDEX_CON_PROP`
--

DROP TABLE IF EXISTS `DTS_QUALIFIED_INDEX_CON_PROP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_QUALIFIED_INDEX_CON_PROP` (
  `property_qualifier_iid` bigint(20) NOT NULL,
  `concept_property_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `qualifier_value` varchar(749) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`property_qualifier_iid`),
  KEY `qual_index_con_prop_iid_idx` (`concept_property_iid`),
  KEY `qual_index_con_prop_pfk` (`qualifier_gid`),
  CONSTRAINT `qual_index_con_prop_cfk` FOREIGN KEY (`concept_property_iid`) REFERENCES `DTS_INDEXABLE_CONCEPT_PROPERTY` (`concept_property_iid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `qual_index_con_prop_pfk` FOREIGN KEY (`qualifier_gid`) REFERENCES `DTS_QUALIFIER_TYPE` (`type_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_QUALIFIER_TYPE`
--

DROP TABLE IF EXISTS `DTS_QUALIFIER_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_QUALIFIER_TYPE` (
  `type_gid` bigint(20) NOT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `qualifies` char(2) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`type_gid`),
  UNIQUE KEY `unique_qualifier_code` (`namespace_id`,`code`),
  UNIQUE KEY `unique_qualifier_id` (`namespace_id`,`id`),
  UNIQUE KEY `unique_qualifier_name` (`namespace_id`,`name`(255)),
  CONSTRAINT `qualifier_namespace_fk` FOREIGN KEY (`namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_QUALIFIER_TYPE_ARCHIVE`
--

DROP TABLE IF EXISTS `DTS_QUALIFIER_TYPE_ARCHIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_QUALIFIER_TYPE_ARCHIVE` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `type_gid` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `qualifies` char(2) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `arch_qualifier_type_vin_fk` (`version_in`),
  KEY `arch_qualifier_type_vout_fk` (`version_out`),
  CONSTRAINT `arch_qualifier_type_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `arch_qualifier_type_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Qual_Big_Con_Prop_Arch`
--

DROP TABLE IF EXISTS `DTS_Qual_Big_Con_Prop_Arch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Qual_Big_Con_Prop_Arch` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) DEFAULT NULL,
  `property_qualifier_iid` bigint(20) NOT NULL,
  `concept_property_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `qualifier_value` varchar(749) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `qual_big_con_prop_vin_fk` (`version_in`),
  KEY `qual_big_con_prop_vout_fk` (`version_out`),
  CONSTRAINT `qual_big_con_prop_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `qual_big_con_prop_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Qual_Index_Con_Prop_Arch`
--

DROP TABLE IF EXISTS `DTS_Qual_Index_Con_Prop_Arch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Qual_Index_Con_Prop_Arch` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `property_qualifier_iid` bigint(20) NOT NULL,
  `concept_property_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `qualifier_value` varchar(749) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `QUAL_INDEX_CON_ARCH_CP_IID_IDX` (`concept_property_iid`),
  KEY `qual_index_con_prop_vin_fk` (`version_in`),
  KEY `qual_index_con_prop_vout_fk` (`version_out`),
  CONSTRAINT `qual_index_con_prop_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `qual_index_con_prop_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Qual_Search_Con_Prop_Arch`
--

DROP TABLE IF EXISTS `DTS_Qual_Search_Con_Prop_Arch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Qual_Search_Con_Prop_Arch` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) DEFAULT NULL,
  `property_qualifier_iid` bigint(20) NOT NULL,
  `concept_property_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `qualifier_value` varchar(749) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `qual_search_con_prop_vin_fk` (`version_in`),
  KEY `qual_search_con_prop_vout_fk` (`version_out`),
  CONSTRAINT `qual_search_con_prop_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `qual_search_con_prop_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Qual_Term_Assn_Archive`
--

DROP TABLE IF EXISTS `DTS_Qual_Term_Assn_Archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Qual_Term_Assn_Archive` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) DEFAULT NULL,
  `association_qualifier_iid` bigint(20) NOT NULL,
  `association_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `value` varchar(749) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `QUAL_TERM_ASSN_ARCH_AIID_IDX` (`association_iid`),
  KEY `QUAL_TERM_ASSN_ARCH_QGID_IDX` (`qualifier_gid`),
  KEY `QUAL_TERM_ASSN_ARCH_QV_IDX` (`value`(255)),
  KEY `qual_term_assn_vin_fk` (`version_in`),
  KEY `qual_term_assn_vout_fk` (`version_out`),
  CONSTRAINT `qual_term_assn_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `qual_term_assn_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Qual_Term_Prop_Archive`
--

DROP TABLE IF EXISTS `DTS_Qual_Term_Prop_Archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Qual_Term_Prop_Archive` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) DEFAULT NULL,
  `property_qualifier_iid` bigint(20) NOT NULL,
  `term_property_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `qualifier_value` varchar(749) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `QUAL_TERM_PROP_ARCH_QGID_IDX` (`qualifier_gid`),
  KEY `QUAL_TERM_PROP_ARCH_QV_IDX` (`qualifier_value`(255)),
  KEY `QUAL_TERM_PROP_ARCH_TPIID_IDX` (`term_property_iid`),
  KEY `qual_term_prop_vin_fk` (`version_in`),
  KEY `qual_term_prop_vout_fk` (`version_out`),
  CONSTRAINT `qual_term_prop_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `qual_term_prop_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Qualified_Big_Con_Prop`
--

DROP TABLE IF EXISTS `DTS_Qualified_Big_Con_Prop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Qualified_Big_Con_Prop` (
  `property_qualifier_iid` bigint(20) NOT NULL,
  `concept_property_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `qualifier_value` varchar(749) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`property_qualifier_iid`),
  KEY `qual_big_con_prop_iid_idx` (`concept_property_iid`),
  KEY `qual_big_con_prop_pfk` (`qualifier_gid`),
  CONSTRAINT `qual_big_con_prop_cfk` FOREIGN KEY (`concept_property_iid`) REFERENCES `DTS_BIG_CONCEPT_PROPERTY` (`concept_property_iid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `qual_big_con_prop_pfk` FOREIGN KEY (`qualifier_gid`) REFERENCES `DTS_QUALIFIER_TYPE` (`type_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Qualified_Con_Assn_Archive`
--

DROP TABLE IF EXISTS `DTS_Qualified_Con_Assn_Archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Qualified_Con_Assn_Archive` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) DEFAULT NULL,
  `association_qualifier_iid` bigint(20) NOT NULL,
  `association_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `value` varchar(749) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `QUAL_CON_ARCH_ASSN_IID_IDX` (`association_iid`),
  KEY `qual_con_assn_vin_fk` (`version_in`),
  KEY `qual_con_assn_vout_fk` (`version_out`),
  CONSTRAINT `qual_con_assn_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `qual_con_assn_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Qualified_Con_Association`
--

DROP TABLE IF EXISTS `DTS_Qualified_Con_Association`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Qualified_Con_Association` (
  `association_qualifier_iid` bigint(20) NOT NULL,
  `association_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `value` varchar(749) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`association_qualifier_iid`),
  KEY `qual_con_assn_assn_iid_idx` (`association_iid`),
  KEY `qualified_con_assn_qfk` (`qualifier_gid`),
  CONSTRAINT `qualified_con_assn_afk` FOREIGN KEY (`association_iid`) REFERENCES `DTS_CONCEPT_ASSOCIATION` (`association_iid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `qualified_con_assn_qfk` FOREIGN KEY (`qualifier_gid`) REFERENCES `DTS_QUALIFIER_TYPE` (`type_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Qualified_Role`
--

DROP TABLE IF EXISTS `DTS_Qualified_Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Qualified_Role` (
  `role_qualifier_iid` bigint(20) NOT NULL,
  `role_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `qualifier_value` varchar(749) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`role_qualifier_iid`),
  KEY `qual_role_role_iid_idx` (`role_iid`),
  KEY `qualified_role_qfk` (`qualifier_gid`),
  CONSTRAINT `qualified_role_qfk` FOREIGN KEY (`qualifier_gid`) REFERENCES `DTS_QUALIFIER_TYPE` (`type_gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `qualified_role_rfk` FOREIGN KEY (`role_iid`) REFERENCES `DTS_COMPLETE_ROLE_CON` (`role_iid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Qualified_Role_Archive`
--

DROP TABLE IF EXISTS `DTS_Qualified_Role_Archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Qualified_Role_Archive` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `role_qualifier_iid` bigint(20) DEFAULT NULL,
  `role_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `qualifier_value` varchar(749) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `qualified_role_vin_fk` (`version_in`),
  KEY `qual_role_vout_fk` (`version_out`),
  CONSTRAINT `qual_role_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `qualified_role_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Qualified_Search_Con_Prop`
--

DROP TABLE IF EXISTS `DTS_Qualified_Search_Con_Prop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Qualified_Search_Con_Prop` (
  `property_qualifier_iid` bigint(20) NOT NULL,
  `concept_property_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `qualifier_value` varchar(749) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`property_qualifier_iid`),
  KEY `qual_search_con_prop_iid_idx` (`concept_property_iid`),
  KEY `qual_search_con_prop_pfk` (`qualifier_gid`),
  CONSTRAINT `qual_search_con_prop_cfk` FOREIGN KEY (`concept_property_iid`) REFERENCES `DTS_SEARCHABLE_CON_PROP` (`concept_property_iid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `qual_search_con_prop_pfk` FOREIGN KEY (`qualifier_gid`) REFERENCES `DTS_QUALIFIER_TYPE` (`type_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Qualified_Term_Association`
--

DROP TABLE IF EXISTS `DTS_Qualified_Term_Association`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Qualified_Term_Association` (
  `association_qualifier_iid` bigint(20) NOT NULL,
  `association_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `value` varchar(749) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`association_qualifier_iid`),
  KEY `qual_term_assn_assn_iid_idx` (`association_iid`),
  KEY `qual_term_assn_qual_gid_idx` (`qualifier_gid`),
  KEY `qual_term_assn_value_idx` (`value`(255)),
  CONSTRAINT `qualified_term_assn_afk` FOREIGN KEY (`association_iid`) REFERENCES `DTS_TERM_ASSOCIATION` (`term_association_iid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `qualified_term_assn_qfk` FOREIGN KEY (`qualifier_gid`) REFERENCES `DTS_QUALIFIER_TYPE` (`type_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Qualified_Term_Property`
--

DROP TABLE IF EXISTS `DTS_Qualified_Term_Property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Qualified_Term_Property` (
  `property_qualifier_iid` bigint(20) NOT NULL,
  `term_property_iid` bigint(20) NOT NULL,
  `qualifier_gid` bigint(20) NOT NULL,
  `qualifier_value` varchar(749) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`property_qualifier_iid`),
  KEY `qual_term_prop_term_prop_idx` (`term_property_iid`),
  KEY `qual_term_prop_qual_gid_idx` (`qualifier_gid`),
  KEY `qual_term_prop_qual_value_idx` (`qualifier_value`(255)),
  CONSTRAINT `qual_term_prop_cfk` FOREIGN KEY (`term_property_iid`) REFERENCES `DTS_TERM_PROPERTY` (`term_property_iid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `qual_term_prop_pfk` FOREIGN KEY (`qualifier_gid`) REFERENCES `DTS_QUALIFIER_TYPE` (`type_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_ROLE_TYPE`
--

DROP TABLE IF EXISTS `DTS_ROLE_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_ROLE_TYPE` (
  `type_gid` bigint(20) NOT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `domain_kind_gid` bigint(20) NOT NULL,
  `range_kind_gid` bigint(20) NOT NULL,
  `right_identity_gid` bigint(20) DEFAULT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  `parent_gid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`type_gid`),
  UNIQUE KEY `unique_role_code` (`namespace_id`,`code`),
  UNIQUE KEY `unique_role_id` (`namespace_id`,`id`),
  UNIQUE KEY `unique_role_name` (`namespace_id`,`name`(255)),
  KEY `domain_kind_fk` (`domain_kind_gid`),
  KEY `range_kind_fk` (`range_kind_gid`),
  KEY `right_identify_fk` (`right_identity_gid`),
  KEY `parent_gid_fk` (`parent_gid`),
  CONSTRAINT `domain_kind_fk` FOREIGN KEY (`domain_kind_gid`) REFERENCES `DTS_Kind` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parent_gid_fk` FOREIGN KEY (`parent_gid`) REFERENCES `DTS_ROLE_TYPE` (`type_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `range_kind_fk` FOREIGN KEY (`range_kind_gid`) REFERENCES `DTS_Kind` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `right_identify_fk` FOREIGN KEY (`right_identity_gid`) REFERENCES `DTS_ROLE_TYPE` (`type_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `role_namespace_fk` FOREIGN KEY (`namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_ROLE_TYPE_ARCHIVE`
--

DROP TABLE IF EXISTS `DTS_ROLE_TYPE_ARCHIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_ROLE_TYPE_ARCHIVE` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `type_gid` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `domain_kind_gid` bigint(20) NOT NULL,
  `range_kind_gid` bigint(20) NOT NULL,
  `right_identity_gid` bigint(20) DEFAULT NULL,
  `parent_gid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `role_type_vin_fk` (`version_in`),
  KEY `role_type_vout_fk` (`version_out`),
  CONSTRAINT `role_type_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `role_type_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_SEARCHABLE_CON_PROP`
--

DROP TABLE IF EXISTS `DTS_SEARCHABLE_CON_PROP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_SEARCHABLE_CON_PROP` (
  `concept_property_iid` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `property_gid` bigint(20) NOT NULL,
  `value` varchar(4000) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`concept_property_iid`),
  KEY `dts_search_prop_cgid_idx` (`concept_gid`),
  KEY `dts_search_prop_pgid_idx` (`property_gid`),
  CONSTRAINT `search_con_prop_cfk` FOREIGN KEY (`concept_gid`) REFERENCES `DTS_Concept` (`concept_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `search_con_prop_pfk` FOREIGN KEY (`property_gid`) REFERENCES `DTS_PROPERTY_TYPE` (`type_gid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_SEARCHABLE_CON_PROP_ARCH`
--

DROP TABLE IF EXISTS `DTS_SEARCHABLE_CON_PROP_ARCH`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_SEARCHABLE_CON_PROP_ARCH` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `concept_property_iid` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `property_gid` bigint(20) NOT NULL,
  `value` varchar(4000) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `dts_search_prop_arch_cgid_idx` (`concept_gid`),
  KEY `dts_search_prop_arch_pgid_idx` (`property_gid`),
  KEY `searchable_con_prop_vin_fk` (`version_in`),
  KEY `searchable_con_prop_vout_fk` (`version_out`),
  CONSTRAINT `searchable_con_prop_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `searchable_con_prop_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_SYNONYM_CLASS`
--

DROP TABLE IF EXISTS `DTS_SYNONYM_CLASS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_SYNONYM_CLASS` (
  `concept_gid` bigint(20) NOT NULL,
  `association_gid` bigint(20) NOT NULL,
  `term_gid` bigint(20) NOT NULL,
  `preferred_term_flag` char(1) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`concept_gid`,`association_gid`,`term_gid`),
  KEY `dts_syn_class_term_idx` (`term_gid`),
  KEY `dts_syn_class_assn_idx` (`association_gid`),
  CONSTRAINT `synonym_class_afk` FOREIGN KEY (`association_gid`) REFERENCES `DTS_ASSOCIATION_TYPE` (`type_gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `synonym_class_cfk` FOREIGN KEY (`concept_gid`) REFERENCES `DTS_Concept` (`concept_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `synonym_class_tfk` FOREIGN KEY (`term_gid`) REFERENCES `DTS_Term` (`term_gid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Synonym_Class_Archive`
--

DROP TABLE IF EXISTS `DTS_Synonym_Class_Archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Synonym_Class_Archive` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `association_gid` bigint(20) NOT NULL,
  `term_gid` bigint(20) NOT NULL,
  `preferred_term_flag` char(1) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `SYN_CLASS_ARCH_ASSN_GID_IDX` (`association_gid`),
  KEY `SYN_CLASS_ARCH_CON_GID_IDX` (`concept_gid`),
  KEY `SYN_CLASS_ARCH_TERM_GID_IDX` (`term_gid`),
  KEY `arch_synonym_vin_fk` (`version_in`),
  KEY `arch_synonym_vout_fk` (`version_out`),
  CONSTRAINT `arch_synonym_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `arch_synonym_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_TERM_ASSOCIATION`
--

DROP TABLE IF EXISTS `DTS_TERM_ASSOCIATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_TERM_ASSOCIATION` (
  `term_association_iid` bigint(20) NOT NULL,
  `from_term_gid` bigint(20) NOT NULL,
  `association_gid` bigint(20) NOT NULL,
  `to_term_gid` bigint(20) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`term_association_iid`),
  UNIQUE KEY `unique_term_assoc` (`from_term_gid`,`association_gid`,`to_term_gid`),
  KEY `dts_term_assoc_from_term_idx` (`from_term_gid`),
  KEY `dts_term_assoc_to_term_idx` (`to_term_gid`),
  KEY `term_assoc_from_to_term_idx` (`from_term_gid`,`to_term_gid`),
  KEY `term_assn_afk` (`association_gid`),
  CONSTRAINT `term_assn_afk` FOREIGN KEY (`association_gid`) REFERENCES `DTS_ASSOCIATION_TYPE` (`type_gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `term_assn_from_tfk` FOREIGN KEY (`from_term_gid`) REFERENCES `DTS_Term` (`term_gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `term_assn_to_tfk` FOREIGN KEY (`to_term_gid`) REFERENCES `DTS_Term` (`term_gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_TERM_ASSOCIATION_ARCHIVE`
--

DROP TABLE IF EXISTS `DTS_TERM_ASSOCIATION_ARCHIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_TERM_ASSOCIATION_ARCHIVE` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `term_association_iid` bigint(20) DEFAULT NULL,
  `from_term_gid` bigint(20) NOT NULL,
  `association_gid` bigint(20) NOT NULL,
  `to_term_gid` bigint(20) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `TERM_ASSN_ARCH_ASSN_IID_IDX` (`term_association_iid`),
  KEY `TERM_ASSN_ARCH_FROM_GID_IDX` (`from_term_gid`),
  KEY `TERM_ASSN_ARCH_TO_GID_IDX` (`to_term_gid`),
  KEY `arch_term_assn_vin_fk` (`version_in`),
  KEY `arch_term_assn_vout_fk` (`version_out`),
  CONSTRAINT `arch_term_assn_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `arch_term_assn_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_TERM_PROPERTY`
--

DROP TABLE IF EXISTS `DTS_TERM_PROPERTY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_TERM_PROPERTY` (
  `term_property_iid` bigint(20) NOT NULL,
  `term_gid` bigint(20) NOT NULL,
  `property_gid` bigint(20) NOT NULL,
  `value` varchar(749) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`term_property_iid`),
  KEY `term_prop_term_gid_idx` (`term_gid`),
  KEY `term_prop_prop_gid_idx` (`property_gid`),
  KEY `term_prop_value_idx` (`value`(255)),
  CONSTRAINT `term_prop_cfk` FOREIGN KEY (`term_gid`) REFERENCES `DTS_Term` (`term_gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `term_prop_pfk` FOREIGN KEY (`property_gid`) REFERENCES `DTS_PROPERTY_TYPE` (`type_gid`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_TERM_PROPERTY_ARCHIVE`
--

DROP TABLE IF EXISTS `DTS_TERM_PROPERTY_ARCHIVE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_TERM_PROPERTY_ARCHIVE` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `term_property_iid` bigint(20) NOT NULL,
  `term_gid` bigint(20) NOT NULL,
  `property_gid` bigint(20) NOT NULL,
  `value` varchar(749) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `TERM_PROP_ARCH_TERM_GID_IDX` (`term_gid`),
  KEY `TERM_PROP_ARCH_IID_IDX` (`term_property_iid`),
  KEY `term_prop_vin_fk` (`version_in`),
  KEY `term_prop_vout_fk` (`version_out`),
  CONSTRAINT `term_prop_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `term_prop_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Term`
--

DROP TABLE IF EXISTS `DTS_Term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Term` (
  `term_gid` bigint(20) NOT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(749) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `value_u` varchar(749) NOT NULL,
  `cur_archive_iid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`term_gid`),
  UNIQUE KEY `unique_term_code` (`namespace_id`,`code`),
  UNIQUE KEY `unique_term_id` (`namespace_id`,`id`),
  KEY `dts_term_name_idx` (`name`(255)),
  KEY `dts_term_id_idx` (`id`),
  KEY `dts_term_code_idx` (`code`),
  KEY `dts_term_value_u_idx` (`value_u`(255)),
  KEY `dts_term_gv_idx` (`term_gid`,`value_u`(255)),
  CONSTRAINT `term_namespace_fk` FOREIGN KEY (`namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Term_Archive`
--

DROP TABLE IF EXISTS `DTS_Term_Archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Term_Archive` (
  `archive_iid` bigint(20) NOT NULL,
  `version_in` bigint(20) NOT NULL,
  `version_out` bigint(20) NOT NULL,
  `term_gid` bigint(20) NOT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(749) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `value_u` varchar(749) NOT NULL,
  PRIMARY KEY (`archive_iid`),
  KEY `dts_term_arch_gid_idx` (`term_gid`,`version_out`),
  KEY `arch_term_vin_fk` (`version_in`),
  KEY `arch_term_vout_fk` (`version_out`),
  CONSTRAINT `arch_term_vin_fk` FOREIGN KEY (`version_in`) REFERENCES `DTS_Version` (`gid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `arch_term_vout_fk` FOREIGN KEY (`version_out`) REFERENCES `DTS_Version` (`gid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DTS_Version`
--

DROP TABLE IF EXISTS `DTS_Version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DTS_Version` (
  `gid` bigint(20) NOT NULL,
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `release_date` datetime(6) NOT NULL,
  PRIMARY KEY (`gid`),
  UNIQUE KEY `unique_version_code` (`namespace_id`,`code`),
  UNIQUE KEY `unique_version_id` (`namespace_id`,`id`),
  UNIQUE KEY `unique_version_name` (`namespace_id`,`name`(255)),
  CONSTRAINT `version_namespace_fk` FOREIGN KEY (`namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GLOBAL_INSTANCE_IDENTIFIER_SEQ`
--

DROP TABLE IF EXISTS `GLOBAL_INSTANCE_IDENTIFIER_SEQ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GLOBAL_INSTANCE_IDENTIFIER_SEQ` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `NS1`
--

DROP TABLE IF EXISTS `NS1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NS1` (
  `concept_gid` bigint(20) NOT NULL,
  `property_gid` bigint(20) NOT NULL,
  `value_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  KEY `NS1_cid_idx` (`concept_gid`),
  KEY `NS1_vid_idx` (`value_id`),
  KEY `NS1_pgid_idx` (`property_gid`),
  KEY `NS1_token_idx` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PERMISSION`
--

DROP TABLE IF EXISTS `PERMISSION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PERMISSION` (
  `USER_NAME` varchar(32) NOT NULL,
  `NAMESPACE_ID` int(11) NOT NULL,
  PRIMARY KEY (`USER_NAME`,`NAMESPACE_ID`),
  KEY `PERMISSION_NAMESPACE_FK` (`NAMESPACE_ID`),
  CONSTRAINT `PERMISSION_NAMESPACE_FK` FOREIGN KEY (`NAMESPACE_ID`) REFERENCES `DTS_Namespace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `PERMISSION_USERNAME_FK` FOREIGN KEY (`USER_NAME`) REFERENCES `USER_INFO` (`USER_NAME`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SEARCH_INDEX`
--

DROP TABLE IF EXISTS `SEARCH_INDEX`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SEARCH_INDEX` (
  `id` bigint(20) NOT NULL,
  `con` bigint(20) NOT NULL,
  `val` varchar(512) DEFAULT NULL,
  `NAMESPACE_ID` int(11) NOT NULL,
  KEY `search_index_id_idx` (`id`),
  KEY `search_index_con_idx` (`con`),
  KEY `search_index_val_idx` (`val`(255))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SEARCH_INDEX_SPEC`
--

DROP TABLE IF EXISTS `SEARCH_INDEX_SPEC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SEARCH_INDEX_SPEC` (
  `name` varchar(256) NOT NULL,
  `id` bigint(20) NOT NULL,
  `PROPERTY_NAME` varchar(256) NOT NULL,
  `PROPERTY_ID` bigint(20) DEFAULT NULL,
  `CONCEPT_NAME` varchar(256) NOT NULL,
  `CONCEPT_ID` bigint(20) DEFAULT NULL,
  `NAMESPACE_ID` int(11) NOT NULL,
  UNIQUE KEY `unique_id` (`id`,`NAMESPACE_ID`),
  UNIQUE KEY `UQ__SEARCH_I__72E12F1B7755B73D` (`name`(255))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `USER_INFO`
--

DROP TABLE IF EXISTS `USER_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USER_INFO` (
  `USER_NAME` varchar(32) NOT NULL,
  `PASSWORD` tinyblob NOT NULL,
  PRIMARY KEY (`USER_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dts_namespace_item_seq`
--

DROP TABLE IF EXISTS `dts_namespace_item_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dts_namespace_item_seq` (
  `namespace_id` int(11) NOT NULL,
  `item_type` char(1) NOT NULL,
  `item_id` int(11) NOT NULL,
  UNIQUE KEY `unique_namespace_item` (`namespace_id`,`item_type`),
  CONSTRAINT `nmsp_item_seq_fk` FOREIGN KEY (`namespace_id`) REFERENCES `DTS_Namespace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dts_sequences`
--

DROP TABLE IF EXISTS `dts_sequences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dts_sequences` (
  `seq` varchar(100) NOT NULL,
  `sequence_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dts_subset`
--

DROP TABLE IF EXISTS `dts_subset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dts_subset` (
  `subset_id` bigint(20) NOT NULL,
  `name` varchar(256) NOT NULL,
  `subset_expression` tinyblob NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `expr_modified_date` datetime(6) NOT NULL,
  `expr_modified_by` varchar(32) DEFAULT NULL,
  `data_load_date` datetime(6) DEFAULT NULL,
  `description` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`subset_id`),
  UNIQUE KEY `unique_subset_name` (`name`(255))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dts_subset_concept`
--

DROP TABLE IF EXISTS `dts_subset_concept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dts_subset_concept` (
  `subset_id` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  KEY `subset_concept_sid_idx` (`subset_id`,`concept_gid`),
  KEY `subset_concept_cgid_idx` (`concept_gid`),
  CONSTRAINT `subset_concept_subset_id_fk` FOREIGN KEY (`subset_id`) REFERENCES `dts_subset` (`subset_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dts_subset_hierarchy`
--

DROP TABLE IF EXISTS `dts_subset_hierarchy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dts_subset_hierarchy` (
  `subset_hierarchy_id` bigint(20) NOT NULL,
  `subset_id` bigint(20) NOT NULL,
  `namespace_id` int(11) NOT NULL,
  `namespace_version` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`subset_hierarchy_id`),
  KEY `subset_hierarchy_subset_fk` (`subset_id`),
  CONSTRAINT `subset_hierarchy_subset_fk` FOREIGN KEY (`subset_id`) REFERENCES `dts_subset` (`subset_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dts_subset_hierarchy_data`
--

DROP TABLE IF EXISTS `dts_subset_hierarchy_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dts_subset_hierarchy_data` (
  `subset_hierarchy_id` bigint(20) NOT NULL,
  `concept_gid` bigint(20) NOT NULL,
  `superconcept_gid` bigint(20) NOT NULL,
  KEY `subset_h_data_idx1` (`subset_hierarchy_id`),
  KEY `subset_h_data_idx2` (`subset_hierarchy_id`,`superconcept_gid`),
  CONSTRAINT `subset_hierarchy_id_fk` FOREIGN KEY (`subset_hierarchy_id`) REFERENCES `dts_subset_hierarchy` (`subset_hierarchy_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dts_subset_ref`
--

DROP TABLE IF EXISTS `dts_subset_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dts_subset_ref` (
  `subset_id` bigint(20) NOT NULL,
  `ref_item_type` char(2) NOT NULL,
  `ref_item_id` bigint(20) NOT NULL,
  UNIQUE KEY `unique_subset_ref` (`subset_id`,`ref_item_id`,`ref_item_type`),
  KEY `subset_ref_item_id_idx` (`ref_item_id`),
  CONSTRAINT `subset_ref_subset_id_fk` FOREIGN KEY (`subset_id`) REFERENCES `dts_subset` (`subset_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-24 15:00:28
