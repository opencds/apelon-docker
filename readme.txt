
#In order to build apelon-app docker container please download apelon-dts-3.5.2.203-linux-mysql.tar.gz from http://apelon-dts.sourceforge.net/download.html and put it inside of apelon-docker/apelon-docker-app/files/

#For the apelon-db you can replace dts-mysql.sql with your own content.   

#Run docker compose:
#Build the images:
docker-compose build

#Run the images:
sudo docker-compose up #Add -d to run them in the background.

#List the running images:
sudo docker-compose ps

#From the previous command you can get the name of the apelon-app image (i.e. apelondocker_apelon-app_1).
#To get the IP where the app is running, you can do:
sudo docker inspect --format '{{ .NetworkSettings.IPAddress }}' apelondocker_apelon-app_1

#Apelon browser will be available on x.x.x.x:8081/dtstreebrowser
#starting server on x.x.x.x:6666



	
