#!/bin/bash

__start_tomcat(){
cd ${APELON_HOME}/bin/browser/; nohup ./runtomcat.sh start &
}

__start_apelon(){
cd ${APELON_HOME}/bin/server; ./StartApelonServer.sh
}

# Call all functions
__start_tomcat
__start_apelon

